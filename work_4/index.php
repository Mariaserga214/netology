<?php

$filepath = 'https://api.openweathermap.org/data/2.5/weather?q=London&APPID=113983b31c84ca1791656d9809ec5959';
$json_data = file_get_contents($filepath) or exit('Не удалось получить данные');
$weather_data = json_decode($json_data, true);

if ($weather_data === NULL) {
	exit('Не удалось расшифровать данные');
}

function checkData($data) { 
	if (empty($data)) { return false; }

	return true;
}

?>

<!DOCTYPE html>
<html>
<head>
	<title> Задание №4 </title>
</head>
<body>
<?php if (checkData($weather_data['main']['temp'])):?>
	<div style="font-weight:bold;"> Температура: <?php echo ($weather_data['main']['temp'] - 273);?></div>
<?php endif;?>
<?php if (checkData($weather_data['weather'])): foreach ($weather_data['weather'] as $key => $item):?>
	<div><?php echo 'Погода: '.'<img src="http://openweathermap.org/img/w/'.$item['icon'].'.png">'.$item['description'];?></div>
<?php endforeach; endif;?>
<?php if (checkData($weather_data['main']['pressure'])):?>
	<div><?php echo 'Давление: '.round(($weather_data['main']['pressure'] /1.33), 0);?></div>
<?php endif;?>

</body>
</html>

