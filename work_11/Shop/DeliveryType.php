<?php

namespace Shop;

interface DeliveryType
{
	public function getName();

	public function getCost($weight);
}
