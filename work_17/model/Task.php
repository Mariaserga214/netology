<?php

class Task
{
    public function findAllByUser($user_id, $order_field)
    {
        $query = Di::get()->db()->prepare(
            "
SELECT task.*, u1.login as user_login, u2.login as assigned_user_login
FROM task 
JOIN user u1 ON task.user_id = u1.id 
JOIN user u2 ON task.assigned_user_id = u2.id 
WHERE task.user_id = :user_id
ORDER BY $order_field DESC"
        );
        $query->bindValue('user_id', $user_id);
        $query->execute();

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findAssignedToUser($user_id, $order_field)
    {
        $query = Di::get()->db()->prepare("
SELECT task.*, u1.login as user_login, u2.login as assigned_user_login
FROM task 
JOIN user u1 ON task.user_id = u1.id 
JOIN user u2 ON task.assigned_user_id = u2.id 
WHERE task.assigned_user_id = :assigned_user_id AND task.user_id != :user_id
ORDER BY $order_field DESC");
        $query->bindValue('assigned_user_id', $user_id);
        $query->bindValue('user_id', $user_id);
        $query->execute();

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function add($user_id, $description)
    {
        $newtask = Di::get()->db()->prepare("INSERT INTO task (description, date_added, assigned_user_id, user_id, is_done) VALUES (:description, NOW(), :assigned_user_id, :user_id, false)");
        $newtask->bindValue ('assigned_user_id', $user_id);
        $newtask->bindValue('user_id', $user_id);
        $newtask->bindValue('description', $description);
        $newtask->execute();
    }

    public function update($task_id, $description)
    {
        $query = Di::get()->db()->prepare('UPDATE task SET description = :description WHERE id = :id');
        $query->bindValue('description', $description);
        $query->bindValue('id', $task_id);
        $query->execute();
    }

    public function complete($task_id)
    {
        $query = Di::get()->db()->prepare('UPDATE task SET is_done = 1 WHERE id = :id;');
        $query->bindValue('id', $task_id);
        $query->execute();
    }

    public function delete($task_id)
    {
        $query = Di::get()->db()->prepare('DELETE FROM task WHERE id = :id;');
        $query->bindValue('id', $task_id);
        $query->execute();
    }

    public function assign($task_id, $user_id)
    {
        $query = Di::get()->db()->prepare('UPDATE task SET assigned_user_id = :assigned_user_id WHERE id = :id;');
        $query->bindValue('assigned_user_id', $user_id);
        $query->bindValue('id', $task_id);
        $query->execute();
    }
}
