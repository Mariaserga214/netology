<?php

require_once __DIR__.'/../vendor/autoload.php';

include __DIR__.'/Di.php';

error_reporting(E_ALL);
ini_set('dispay_errors', 1);

session_start();

$user = 'root';
$pass = '';
$pdo = new PDO('mysql:host=localhost;dbname=netology', $user, $pass);

$task = new Task();

$is_edit = false;

if (count($_POST) && array_key_exists('add', $_POST) && strlen(trim($_POST['description']))) {
    $task->add($_SESSION['user']['id'], $_POST['description']);

    header("Location: ./index.php");
    exit();
}

if (count($_POST) && array_key_exists('edit', $_POST) && strlen(trim($_POST['description']))) {
    $task->update($_GET['id'], $_POST['description']);

    header("Location: ./index.php");
    exit();
}

if (array_key_exists('action', $_GET)) {
    switch ($_GET['action']) {
        case 'complete':
            $task->complete($_GET['id']);

            header("Location: ./index.php");
            exit();
            break;
        case 'delete':
            $task->delete($_GET['id']);

            header("Location: ./index.php");
            exit();
            break;
        case 'edit':
            $is_edit = true;
            break;
        case 'assign':
            $task->assign($_GET['id'], $_POST['assigned_user_id']);

            break;
        default:
            break;
    }
}

if (count($_POST) && array_key_exists('sort', $_POST)) {
    $order_field = $_POST['sort_by'];
} else {
    $order_field = 'date_added';
}

$tabledata = $task->findAllByUser($_SESSION['user']['id'], $order_field);
$assigned_data = $task->findAssignedToUser($_SESSION['user']['id'], $order_field);

$user = new User();
$allusers = $user->findAll();

$statuses = [
    0 => 'Не выполнено',
    1 => 'Выполнено',
];

$loader = new Twig_Loader_Filesystem(__DIR__.'/templates');
$twig = new Twig_Environment($loader, array(
    'cache' => __DIR__.'/compilation_cache',
));

echo $twig->render('index.html.twig', [
    'allusers' => $allusers,
    'statuses' => $statuses,
    'order_field' => $order_field,
    'assigned_data' => $assigned_data,
    'tabledata' => $tabledata,
]);
