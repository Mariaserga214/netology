<?php

namespace Catalogue;

interface Discount
{
	public function getDiscount();
	public function getPriceWithDiscount();
}
