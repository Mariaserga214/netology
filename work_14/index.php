<?php

include __DIR__.'/config.php';

error_reporting(E_ALL);
ini_set('dispay_errors', 1);

session_start();

if (!array_key_exists('user', $_SESSION)) {
    header('Location: ./reg.php');
    exit('Вы не авторизованы');
}

$is_edit = false;

if (count($_POST) && array_key_exists('add', $_POST) && strlen(trim($_POST['description']))) {
    $newtask = db()->prepare("INSERT INTO task (description, date_added, assigned_user_id, user_id, is_done) 
      VALUES (:description, NOW(), :assigned_user_id, :user_id, false)");
    $newtask->bindValue ('assigned_user_id', $_SESSION['user']['id']);
    $newtask->bindValue('user_id', $_SESSION['user']['id']);
    $newtask->bindValue('description', $_POST['description']);
    $newtask->execute();

    header("Location: ./index.php");
    exit();
}

if (count($_POST) && array_key_exists('edit', $_POST) && strlen(trim($_POST['description']))) {
    $query = db()->prepare('UPDATE task SET description = :description WHERE id = :id');
    $query->bindValue('description', $_POST['description']);
    $query->bindValue('id', $_GET['id']);
    $query->execute();

    header("Location: ./index.php");
    exit();
}

if (array_key_exists('action', $_GET)) {
    switch ($_GET['action']) {
        case 'complete':
            $query = db()->prepare('UPDATE task SET is_done = 1 WHERE id = :id;');
            $query->bindValue('id', $_GET['id']);
            $query->execute();

            header("Location: ./index.php");
            exit();
            break;
        case 'delete':
            $query = db()->prepare('DELETE FROM task WHERE id = :id;');
            $query->bindValue('id', $_GET['id']);
            $query->execute();

            header("Location: ./index.php");
            exit();
            break;
        case 'edit':
            $is_edit = true;
            break;
        case 'assign':
            $query = db()->prepare('UPDATE task SET assigned_user_id = :assigned_user_id WHERE id = :id;');
                $query->bindValue('assigned_user_id', $_POST['assigned_user_id']);
                $query->bindValue('id', $_GET['id']);
                $query->execute();
            break;
        default:
            break;
    }
}

if (count($_POST) && array_key_exists('sort', $_POST)) {
    $order_field = $_POST['sort_by'];
} else {
    $order_field = 'date_added';
}

$query = db()->prepare("
SELECT task.*, u1.login as user_login, u2.login as assigned_user_login
FROM task 
JOIN user u1 ON task.user_id = u1.id 
JOIN user u2 ON task.assigned_user_id = u2.id 
WHERE task.user_id = :user_id
ORDER BY $order_field DESC");
$query->bindValue('user_id', $_SESSION['user']['id']);
$query->execute();

$query2 = db()->prepare("
SELECT task.*, u1.login as user_login, u2.login as assigned_user_login
FROM task 
JOIN user u1 ON task.user_id = u1.id 
JOIN user u2 ON task.assigned_user_id = u2.id 
WHERE task.assigned_user_id = :assigned_user_id AND task.user_id != :user_id
ORDER BY $order_field DESC");
$query2->bindValue('assigned_user_id', $_SESSION['user']['id']);
$query2->bindValue('user_id', $_SESSION['user']['id']);
$query2->execute();

$query->execute();

$tabledata = $query->fetchAll(PDO::FETCH_ASSOC);
$assigned_data = $query2->fetchAll(PDO::FETCH_ASSOC);

$allusers = db()->prepare("SELECT * from user");
$allusers->execute();

$allusers = $allusers->fetchAll(PDO::FETCH_ASSOC);

$statuses = [
    0 => 'Не выполнено',
    1 => 'Выполнено',
];

?>

<!DOCTYPE html>
<html>
<head>
    <title>Список дел на сегодня</title>
</head>
<body>
<h3>Новое задание</h3>
<div>
    <a href="./exit.php">Выйти</a>
</div>
<br/>
<br/>
<?php if ($is_edit): ?>
    <form action="" method="post">
        <input type="text" name="description" placeholder="Описание задачи"/>
        <input type="submit" name="edit" value="Изменить описание"/>
    </form>
<?php else: ?>
    <form action="" method="post">
        <input type="text" name="description" placeholder="Описание задачи"/>
        <input type="submit" name="add" value="Добавить"/>
    </form>
<?php endif; ?>
<form action="" method="post">
    <label>
        Сортировать по:
        <select name="sort_by">
            <option value="date_added" <?php if ($order_field == 'date_added'): ?> selected="selected"<?php endif; ?>>
                Дате добавления
            </option>
            <option value="is_done" <?php if ($order_field == 'is_done'): ?> selected="selected"<?php endif; ?>>
                Статусу
            </option>
            <option value="description" <?php if ($order_field == 'description'): ?> selected="selected"<?php endif; ?>>
                Описанию
            </option>
        </select>
        <input type="submit" name="sort" value="Сортировать">
    </label>
</form>
<br/>
<br/>
<br/>
<h1> Мои задачи</h1>
<table border="1">
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
        <th>Закрепить задачу за пользователем</th>
    </tr>
    <?php foreach ($tabledata as $data): ?>
        <tr>
            <td><?php echo $data['description']; ?></td>
            <td><?php echo $data['date_added']; ?></td>
            <td><?php echo $statuses[$data['is_done']]; ?></td>
            <td>
                <a href="?action=edit&amp;id=<?php echo $data['id']; ?>">Изменить</a>
                <?php if ($data['is_done'] == false): ?>
                    <a href="?action=complete&amp;id=<?php echo $data['id']; ?>">Выполнить</a>
                <?php endif; ?>
                <a href="?action=delete&amp;id=<?php echo $data['id']; ?>">Удалить</a></td>
            <td><?php echo $data['assigned_user_login']; ?></td>
            <td><?php echo $data['user_login']; ?></td>
            <td>
                <form method="post" action="?action=assign&amp;id=<?php echo $data['id']; ?>">
                    <select name="assigned_user_id">
                    <?php foreach ($allusers as $user):?>

                    <option value="<?php echo $user['id'];?>"> <?php echo $user['login'];?> </option>
                    <?php endforeach ;?>
                </select>
                <input type="submit" name="assign" value="Назначить ответственного">
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<h1>
    Назначенные задачи
</h1>
<table border="1">
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
        <th>Закрепить задачу за пользователем</th>
    </tr>
    <?php foreach ($assigned_data as $data): ?>
        <tr>
            <td><?php echo $data['description']; ?></td>
            <td><?php echo $data['date_added']; ?></td>
            <td><?php echo $statuses[$data['is_done']]; ?></td>
            <td>
                <a href="?action=edit&amp;id=<?php echo $data['id']; ?>">Изменить</a>
                <?php if ($data['is_done'] == false): ?>
                    <a href="?action=complete&amp;id=<?php echo $data['id']; ?>">Выполнить</a>
                <?php endif; ?>
                <a href="?action=delete&amp;id=<?php echo $data['id']; ?>">Удалить</a></td>
            <td><?php echo $data['assigned_user_login']; ?></td>
            <td><?php echo $data['user_login']; ?></td>
            <td>
                <form method="post" action="?action=assign&amp;id=<?php echo $data['id']; ?>">
                    <select name="assigned_user_id">
                        <?php foreach ($allusers as $user):?>

                            <option value="<?php echo $user['id'];?>"> <?php echo $user['login'];?> </option>
                        <?php endforeach ;?>
                    </select>
                    <input type="submit" name="assign" value="Назначить ответственного">
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
</body>
</html>
