<?php

namespace Catalogue;

abstract class Product implements Discount, DeliveryOption
{
    public $name;
    public $category;
    public $availability;
    public $price;
    public $deliveryprice;
    public $weight;
    public $discount = 0;

    public function __construct($name, $category, $availability, $price, $deliveryprice, $weight, $discount = null)
    {
        $this->name = $name;
        $this->category = $category;
        $this->availability = $availability;
        $this->price = $price;
        $this->deliveryprice = $deliveryprice;
        $this->weight = $weight;

        if ($discount) {
            $this->discount = $discount;
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPriceWithDiscount()
    {
        if ($this->discount) {
            return $this->price - ($this->price * $this->discount / 100);
        }

        return $this->price;
    }

    public function canBePurchased()
    {
        return $this->availability == 'yes' && $this->price > 0;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function orderDelivery()
    {
        return 'pickup';
    }

    public function setDiscount($discount)
    {
        if ($discount < 0) {
            $discount = 0;
        }

        if ($discount > 100) {
            $discount = 100;
        }

        $this->discount = $discount;
    }

    public function getDeliveryPrice()
    {
        if ($this->discount) {
            return 300;
        }

        return 250;
    }

    public function getweight()
    {
        return $this->weight;
    }

    public function __toString()
    {
        $text = '';

        $text .= 'Название: '.$this->name."\n";
        $text .= 'Категория: '.$this->category."\n";
        $text .= 'Цена: '.$this->price."\n";
        $text .= 'Стоимость доставки: '.$this->deliveryprice."\n";
        $text .= 'Вес: '.$this->weight."\n";
        $text .= 'Скидка: '.$this->discount."\n";

        return $text;
    }
}
