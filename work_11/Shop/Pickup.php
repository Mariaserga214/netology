<?php

namespace Shop;

class Pickup implements DeliveryType
{
    public $name;

    public function getName()
    {
        return $this->name;
    }

    public function getCost($weight)
    {
        return $weight * 2;
    }

    public function __toString()
    {
        return 'Самовывоз';
    }
}
