<?php

namespace Shop;

class Curier implements DeliveryType
{
    public $name;

    public function getName()
    {
        return $this->name;
    }

    public function getCost($weight)
    {
        return $weight * 1.3;
    }

    public function __toString()
    {
        return 'Курьер';
    }
}
