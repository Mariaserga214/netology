<?php

$host = 'localhost';
$user = 'root';
$password = '';
$database = 'netology';

$link = mysqli_connect($host, $user, $password, $database)
or die("Ошибка ".mysqli_error($link));

if (array_key_exists('create', $_GET)) {
    $query = "CREATE Table schedule
(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(200) NOT NULL,
    task VARCHAR (200) NOT NULL,
    deadline DATETIME NOT NULL,
    responsible VARCHAR (200) NOT NULL,
    author VARCHAR (200) NOT NULL,
    is_complete BOOLEAN NOT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL
)";

    $result = mysqli_query($link, $query) or die("Ошибка ".mysqli_error($link));

    if ($result) {
        echo "Таблица создана";
    }

    mysqli_close($link);
}

$query = 'SHOW TABLES;';

$result = mysqli_query($link, $query);

//while ($tables = $result->fetch_assoc()) {
//    print_r($xx);
// $tables['Tables_in_netology']
//}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Список таблиц</title>
</head>
<body>
<table border="1">
    <tr>
        <th>Название</th>
        <th>Ссылка</th>
        <th>Дата добавления</th>
        <th>Автор</th>

    </tr>
    <?php while ($tables = $result->fetch_assoc()): ?>
        <tr>
            <td><?php echo $tables['Tables_in_netology']; ?></td>
            <td>
                <a href="./table.php?name=<?php echo $tables['Tables_in_netology']; ?>">Посмотреть</a>




            </td>
        </tr>
    <?php endwhile; ?>
</table>

</body>
</html>


