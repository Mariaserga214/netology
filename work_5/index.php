<?php

$filepath = __DIR__."/data.json";
$json_data = file_get_contents($filepath);
$contacts = json_decode($json_data, true);

?>

<!DOCTYPE html>
<html>
<head>
	<title> Задание №5 </title>
</head>
<body>
<table border="1">
   <tr>
      <th>Имя</th>
      <th>Фамилия</th>
      <th>Адрес</th>
      <th>Телефон</th>
    </tr>
		<?php foreach ($contacts as $tableHead => $description):?>
			<tr>
				<td ><?php echo ($description['name']);?></td>
				<td><?php echo ($description['lastName']);?></td>
				<td><?php echo ($description['address']);?></td>
				<td><?php echo ($description['phoneNumber']);?></td>
			</tr>
		<?php endforeach;?>
    
</table>
</body>
</html>
  

