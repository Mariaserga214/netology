<?php

require __DIR__.'/../vendor/autoload.php';

$collection = [];

if (count($_GET) && array_key_exists('address', $_GET)) {
    $api = new \Yandex\Geo\Api();

    $api->setQuery($_GET['address']);

    $api
        ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
        ->load();

    $response = $api->getResponse();
    $response->getFoundCount(); // кол-во найденных адресов
    $response->getQuery(); // исходный запрос
    $response->getLatitude(); // широта для исходного запроса
    $response->getLongitude(); // долгота для исходного запроса

    $collection = $response->getList();

    foreach ($collection as $item) {
        $item->getAddress(); // вернет адрес
        $item->getLatitude(); // широта
        $item->getLongitude(); // долгота
        $item->getData(); // необработанные данные
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Поиск адреса</title>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>
<body>
<h2>Введите адрес</h2>

<form action="" method="get">
    <input type="text" name="address" placeholder="Введите адрес" value="<?php if (array_key_exists('address', $_POST)): ?><?php echo $_POST['address']; ?><?php endif; ?>"/>
    <input type="submit" name="find" value="Найти адрес"/>
</form>

<br/>
<br/>
<br/>
<br/>

<?php if (count($collection) == 1 || (array_key_exists('key', $_GET) && array_key_exists($_GET['key'], $collection))): ?>
    <div id="map" style="width: 600px; height: 400px"></div>
<?php endif; ?>

<br/>
<br/>

<?php foreach ($collection as $key => $item) : ?>
    <div>
        <h3>
            <a href="?key=<?php echo $key; ?>&address=<?php echo $_GET['address']; ?>">
                Адрес №<?php echo $key + 1; ?>
            </a>
        </h3>
        <div>Адрес: <?php echo $item->getAddress(); ?></div>
        <div>Широта: <?php echo $item->getLatitude(); ?></div>
        <div>Долгота: <?php echo $item->getLongitude(); ?></div>
    </div>
<?php endforeach ?>

<?php if (count($collection) == 1 || (array_key_exists('key', $_GET) && array_key_exists($_GET['key'], $collection))): ?>
    <?php
        if (count($collection) == 1) {
            $item = $collection[0];
        } elseif ((array_key_exists('key', $_GET) && array_key_exists($_GET['key'], $collection))) {
            $item = $collection[$_GET['key']];
        }
    ?>

    <script type="text/javascript">
        ymaps.ready(init);

        function init() {
            var myMap = new ymaps.Map("map", {
                center: [55.76, 37.64],
                zoom: 3
            });

            var myPlacemark = new ymaps.Placemark([<?php echo $item->getLatitude(); ?>, <?php echo $item->getLongitude(); ?>], {
                hintContent: 'Адрес',
                balloonContent: '<?php echo $item->getAddress(); ?>'
            });
            myMap.geoObjects.add(myPlacemark);
        }
    </script>
<?php endif; ?>
</body>
</html>
