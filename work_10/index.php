<?php

interface DeliveryOption
{
    public function orderDelivery();
}

interface Discount
{
    public function getDiscount();

    public function getPriceWithDiscount();
}

interface Promo
{
    public function getPromo();
}

abstract class ShopItem//суперкласс
{
    public $name;
    public $price;
    public $availability;
    public $discount;

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getAvailability()
    {
        return $this->availability;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getPriceWithDiscount()
    {
        if ($this->discount) {
            return $this->price - $this->discount;
        }

        return $this->price;
    }
}

//Первый класс "Машина"
class Car extends ShopItem implements DeliveryOption
{
    public $model;
    public $color;
    public $maxspeed;
    public $year;

    public function __construct($model, $color, $maxspeed, $year, $price, $discount = null)
    {
        $this->model = $model;
        $this->color = $color;
        $this->maxspeed = $maxspeed;
        $this->year = $year;
        $this->price = $price;

        if ($discount) {
            $this->discount = $discount;
        }
    }

    public function setMaxSpeed($maxspeed)
    {
        if ($maxspeed < 0) {
            $maxspeed = 0;
        }

        $this->maxspeed = $maxspeed;
    }

    public function getMaxSpeedWithName()
    {
        return $this->maxspeed.'km/h';
    }

    public function orderDelivery()
    {
        return 'Pickup';
    }
}

//Объекты Машина

$car1 = new Car('Audi A7', 'red', 250, '2012', 3200000);
$car2 = new Car('BMW X3', 'black', 245, '2014', 3500000);


echo $car1->getPrice()."\n";
print_r($car1);
echo 'Первое авто'."\br";

print_r($car2);

$current_price = 0;
$cars = [$car1, $car2];


foreach ($cars as $car) {
    if ($car->getPrice() > $current_price) {
        $current_price = $car->getPrice();
    }
}

echo $current_price."\n";

//Второй класс "Телевизор"

class TVset extends ShopItem implements Discount
{
    public $model;
    public $color;
    public $diagonal;
    public $weight;

    public function __construct($model, $color, $diagonal, $price, $weight, $discount = null)
    {
        $this->model = $model;
        $this->color = $color;
        $this->diagonal = $diagonal;
        $this->price = $price;
        $this->weight = $weight;

        if ($discount && $weight > 10) {
            $this->discount = $discount;
        }
    }

    public function isWidescreen()
    {
        return $this->diagonal > 45;
    }
}

//Объекты телевизор
$TVset1 = new TVset('Philips', 'grey', '50', 40000, 7);
$TVset2 = new TVset('Samsung', 'black', '40', 27000, 11, 10);
$TVset3 = new TVset('Samsung', 'white', '30', 15000, 4, 15);

echo $TVset1->getPriceWithDiscount()."\n"; //-- товар без скидки
echo $TVset2->getPriceWithDiscount()."\n"; //-- товар со скидкой
echo $TVset3->getPriceWithDiscount()."\n"; //-- товар со скидкой, но вес меньше 10
print_r($TVset1);
print_r($TVset2);

//Третий класс "Шариковая ручка"	
class Pen extends SHopItem implements DeliveryOption
{
    public $label;
    public $color;
    public $diameter;

    public function __construct($label, $color, $diameter, $price, $discount = null)
    {
        $this->label = $label;
        $this->color = $color;
        $this->diameter = $diameter;
        $this->price = $price;

        if ($discount) {
            $this->discount = $discount;
        }
    }

    public function isForStudent()
    {
        return $this->color == 'blue';
    }

    public function orderDelivery()
    {
        return 'Delivery';
    }
}

$Pen1 = new Pen('BIC', 'blue', '0,9', 30);
$Pen2 = new Pen('Pilot', 'red', '0,6', 20);

echo $Pen1->getPrice()."\n";
print_r($Pen1);
print_r($Pen2);

//Четвертый класс "Утка"	
class Duck extends ShopItem implements Promo
{
    public $material;
    public $color;
    public $promo; //наличие подарка или нет

    public function __construct($material, $color, $promo, $price, $discount = null)
    {
        $this->material = $material;
        $this->color = $color;
        $this->promo = $promo;
        $this->price = $price;

        if ($discount) {
            $this->discount = $discount;
        }
    }

    public function isPromo()
    {
        return $this->promo == 'yes';
    }

    public function getPromo()
    {
        return 'Promo is available';
    }
}

$Duck1 = new Duck('rubber', 'yellow', 'no', 100);
$Duck2 = new Duck('porcelain', 'white', 'yes', 1200);


echo $Duck1->getPrice()."\n";
print_r($Duck1);
print_r($Duck2);

//Пятый класс "Товар"	
class Product extends ShopItem implements Discount, DeliveryOption
{
    public $category;
    public $deliveryprice;
    public $weight;

    public function __construct($name, $category, $availability, $price, $deliveryprice, $weight, $discount = null)
    {
        $this->name = $name;
        $this->category = $category;
        $this->availability = $availability;
        $this->price = $price;
        $this->deliveryprice = $deliveryprice;
        $this->weight = $weight;

        if ($discount) {
            $this->discount = $discount;
        }
    }

    public function getPriceWithDiscount()
    {
        if ($this->discount) {
            return $this->price - ($this->price * $this->discount / 100);
        }

        return $this->price;
    }

    public function canBePurchased()
    {
        return $this->availability == 'yes' && $this->price > 0;
    }

    public function orderDelivery()
    {
        return 'pickup';
    }

    public function setDiscount($discount)
    {
        if ($discount < 0) {
            $discount = 0;
        }

        if ($discount > 100) {
            $discount = 100;
        }

        $this->discount = $discount;
    }

    public function getDeliveryPrice()
    {
        if ($this->discount) {
            return 300;
        }

        return 250;
    }

}

$Product1 = new Product('Armchair Blue Sky', 'furniture', 'available', 12000, null, 12, 10);
$Product2 = new Product('Table Greenwood', 'furniture', 'not available', 5000, null, 11, 10);
$Product3 = new Product('Chair Sweet Melody', 'furniture', 'available', 2700, null, 5, 0);


echo $Product1->getPriceWithDiscount()."\n";
echo $Product2->getPriceWithDiscount()."\n";
echo $Product3->getPriceWithDiscount()."\n";

echo $Product1->getDeliveryPrice()."\n";
echo $Product2->getDeliveryPrice()."\n";
echo $Product3->getDeliveryPrice()."\n";
