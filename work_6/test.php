<?php

if (!array_key_exists('name', $_GET)) {
	exit('Не указано имя нужного теста!');
}

$filename = $_GET['name'];
$tests_dir = __DIR__.'/tests/';
$test_path = $tests_dir.$filename.'.json';

if (!file_exists($test_path)) {
	exit('Теста с таким именем не найдено!');
}

$data = file_get_contents($test_path);
$test_data = json_decode($data, true);

$result = '';

if (array_key_exists('submit', $_POST) && array_key_exists('question', $_POST) && is_array($_POST['question'])) {
	$true_answers_count = 0;

	foreach ($_POST['question'] as $question_id => $answer_id) {
		if ($test_data[$question_id]['answers'][$answer_id]['is_true']) {
			$true_answers_count++;
		}
	}

	$result = 'Вы ответили верно на '.$true_answers_count.' вопросов из '.count($test_data);
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Тест <?php echo $filename; ?></title>
</head>
<body>

<?php if ($result): ?>
	<h1><?php echo $result; ?></h1>
<?php endif; ?>

<form method="post" action="">
	<?php foreach ($test_data as $question_id => $question): ?>
		<div>
			<h3><?php echo $question['question']; ?></h3>
			<?php foreach ($question['answers'] as $answer_id => $answer): ?>
				<div>
					<label>
						<input type="radio" name="question[<?php echo $question_id; ?>]" required="required" value="<?php echo $answer_id; ?>" />
						<?php echo $answer['text']; ?>
					</label>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endforeach; ?>
	<br/>
	<br/>
	<input type="submit" name="submit" value="Отправить"/>
</form>

</body>
</html>
