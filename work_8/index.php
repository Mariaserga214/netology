<?php

session_start();

function getUser(string $login): ?array
{
    if (!file_exists($file = __DIR__.'/users/'.$login.'.json')) {
        return null;
    }

    $userdata = file_get_contents(__DIR__.'/users/'.$login.'.json');

    if ($userdata === false) {
        return null;
    }

    $userdata = json_decode($userdata, true);

    if (!$userdata) {
        return null;
    }

    return $userdata;
}

function login(string $login, string $password): bool
{
    $user = getUser($login);

    if ($user && $user['password'] == $password) {
        $_SESSION['user'] = $user;

        return true;
    }

    return false;
}

$message = '';

if (!empty($_POST)) {
    if (array_key_exists('is_guest', $_POST)) {
        header('Location: ./list.php');
    }

    if (array_key_exists('login', $_POST) && array_key_exists('password', $_POST)) {
        if (login($_POST['login'], $_POST['password'])) {
            header('Location: ./list.php');
        }
    }

    $message = 'Неверно введен пароль!';
}

?>


<!doctype html>
<html lang="en">
<head>
    <title>Войти для прохождени тестов</title>
</head>
<body>
<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-xs-wrap">
                <h1>Войти для прохождения тестов</h1>

                <?php echo($message); ?>

                <form method="POST">
                    <div class="form-group">
                        <label for="lg" class="sr-only">Логин</label>
                        <input type="text" placeholder="Логин" name="login" id="lg" class="form-control"
                               required="required">
                    </div>
                    <div class="form-group">
                        <label for="key" class="sr-only">Пароль</label>
                        <input type="password" placeholder="Пароль" name="password" id="key" class="form-control">
                    </div>
                    <input type="submit" id="btn-login" class="btn btn-success btn-lg btn-block" value="Войти">
                    <input type="checkbox" id="guest" name="is_guest" value="guest"/>
                    <label for="guest">Войти как гость</label>
                </form>
                <hr>
            </div>
        </div>
    </div>
</section>
</body>
</html>
