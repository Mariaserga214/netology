<?php

error_reporting(E_ALL);
ini_set('dispay_errors', 1);

$user = 'root';
$pass = '';
$pdo = new PDO('mysql:host=localhost;dbname=netology', $user, $pass);

$is_edit = false;

if (count($_POST) && array_key_exists('add', $_POST) && strlen(trim($_POST['description']))) {
    $newtask = $pdo->prepare("INSERT INTO tasks (description, date_added, is_done) VALUES (?, NOW(), false)");
    $newtask->bindValue(1, $_POST['description']);
    $newtask->execute();

    header("Location: ./index.php");
    exit();
}

if (count($_POST) && array_key_exists('edit', $_POST) && strlen(trim($_POST['description']))) {
    $query = $pdo->prepare('UPDATE tasks SET description = :description WHERE id = :id');
    $query->bindValue('description', $_POST['description']);
    $query->bindValue('id', $_GET['id']);
    $query->execute();

    header("Location: ./index.php");
    exit();
}

if (array_key_exists('action', $_GET)) {
    switch ($_GET['action']) {
        case 'complete':
            $query = $pdo->prepare('UPDATE tasks SET is_done = 1 WHERE id = :id;');
            $query->bindValue('id', $_GET['id']);
            $query->execute();

            header("Location: ./index.php");
            exit();
            break;
        case 'delete':
            $query = $pdo->prepare('DELETE FROM tasks WHERE id = :id;');
            $query->bindValue('id', $_GET['id']);
            $query->execute();

            header("Location: ./index.php");
            exit();
            break;
        case 'edit':
            $is_edit = true;
            break;
        default:
            break;
    }
}

if (count($_POST) && array_key_exists('sort', $_POST)) {
    $order_field = $_POST['sort_by'];
} else {
    $order_field = 'date_added';
}

$query = $pdo->prepare("SELECT * FROM tasks ORDER BY $order_field DESC");

$query->execute();

$tabledata = $query->fetchAll(PDO::FETCH_ASSOC);

$statuses = [
    0 => 'Не выполнено',
    1 => 'Выполнено',
];

?>

<!DOCTYPE html>
<html>
<head>
    <title>Список дел на сегодня</title>
</head>
<body>
<h3>Новое задание</h3>
<?php if ($is_edit): ?>
    <form action="" method="post">
        <input type="text" name="description" placeholder="Описание задачи"/>
        <input type="submit" name="edit" value="Изменить описание"/>
    </form>
<?php else: ?>
    <form action="" method="post">
        <input type="text" name="description" placeholder="Описание задачи"/>
        <input type="submit" name="add" value="Добавить"/>
    </form>
<?php endif; ?>
<form action="" method="post">
    <label>
        Сортировать по:
        <select name="sort_by">
            <option value="date_added" <?php if ($order_field == 'date_added'): ?> selected="selected"<?php endif; ?>>
                Дате добавления
            </option>
            <option value="is_done" <?php if ($order_field == 'is_done'): ?> selected="selected"<?php endif; ?>>
                Статусу
            </option>
            <option value="description" <?php if ($order_field == 'description'): ?> selected="selected"<?php endif; ?>>
                Описанию
            </option>
        </select>
        <input type="submit" name="sort" value="Сортировать">
    </label>
</form>
<br/>
<br/>
<br/>
<table border="1">
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
    </tr>
    <?php foreach ($tabledata as $data): ?>
        <tr>
            <td><?php echo $data['description']; ?></td>
            <td><?php echo $data['date_added']; ?></td>
            <td><?php echo $statuses[$data['is_done']]; ?></td>
            <td>
                <a href="?action=edit&amp;id=<?php echo $data['id']; ?>">Изменить</a>
                <?php if ($data['is_done'] == false): ?>
                    <a href="?action=complete&amp;id=<?php echo $data['id']; ?>">Выполнить</a>
                <?php endif; ?>
                <a href="?action=delete&amp;id=<?php echo $data['id']; ?>">Удалить</a></td>
        </tr>
    <?php endforeach; ?>
</body>
</html>
