<?php

namespace Catalogue;

class Pen extends Product implements DeliveryOption
{
    public $label;
    public $color;
    public $diameter;

    public function __construct($label, $color, $diameter, $price, $discount = null)
    {
        parent::__construct($label, 'pen', true, $price, 150, 2, $discount);

        $this->label = $label;
        $this->color = $color;
        $this->diameter = $diameter;

        if ($discount) {
            $this->discount = $discount;
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPriceWithDiscount()
    {
        if ($this->discount) {
            return $this->price - $this->discount;
        }

        return $this->price;
    }

    public function isForStudent()
    {
        return $this->color == 'blue';
    }

    public function orderDelivery()
    {
        return 'Delivery';
    }
}
