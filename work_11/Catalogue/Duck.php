<?php

namespace Catalogue;

class Duck extends Product implements Promo
{
    public $material;
    public $color;
    public $promo;

    public function __construct($name, $material, $color, $promo, $price, $discount = null)
    {
        parent::__construct($name, 'duck', true, $price, 100, 1, $discount);

        $this->material = $material;
        $this->color = $color;
        $this->promo = $promo;

        if ($discount) {
            $this->discount = $discount;
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPriceWithDiscount()
    {
        if ($this->discount) {
            return $this->price - $this->discount;
        }

        return $this->price;
    }

    public function isPromo()
    {
        return $this->promo == 'yes';
    }

    public function getPromo()
    {
        return 'Promo is available';
    }
}
