<?php

require_once __DIR__.'/../vendor/autoload.php';

include __DIR__.'/Di.php';

session_start();

$message = '';

if (!empty($_POST)) {
    if (array_key_exists('login', $_POST) && array_key_exists('password', $_POST)) {
        $user = new User();

        $userdata = $user->findByLoginAndPassword($_POST['login'], $_POST['password']);

        if ($userdata) {

            $_SESSION['user'] = $userdata[0];
            header('Location: ./index.php');
        }

        $message = 'Неверно введен пароль!';
    }
}

$loader = new Twig_Loader_Filesystem(__DIR__.'/templates');
$twig = new Twig_Environment($loader, array(
    'cache' => __DIR__.'/compilation_cache',
));

echo $twig->render('reg.html.twig', [
    'message' => $message,
]);
