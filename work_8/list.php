<?php

session_start();

$save_path = __DIR__.'/tests/';

$uploaded_files = scandir($save_path);

$tests = [];

foreach ($uploaded_files as $filename) {
    if (strpos($filename, '.') === 0) {
        continue;
    }

    $tests[pathinfo($filename, PATHINFO_FILENAME)] = $filename;
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Список загруженных тестов</title>
</head>
<body>
<?php if (array_key_exists('user', $_SESSION)): ?>
    <a href="./admin.php">Добавить тест</a>
<?php endif; ?>
<a href="./exit.php">Выйти</a>
<ul>
    <?php foreach ($tests as $key => $test): ?>
        <li>
            <a href="./test.php?name=<?php echo $key; ?>"><?php echo $test; ?></a>
            <?php if (array_key_exists('user', $_SESSION)): ?>
                <a href="./delete_test.php?name=<?php echo $test; ?>">Удалить тест</a>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>
</body>
</html>
