<?php

namespace Catalogue;

class PC extends Product
{
    public $model;
    public $frequency;
    public $ddr;

    public function __construct($model, $price, $deliveryprice, $weight, $ddr, $frequency, $discount = null)
    {
        parent::__construct($model, 'PC', true, $price, $deliveryprice, $weight, $discount);

        $this->model = $model;
        $this->frequency = $frequency;
        $this->ddr = $ddr;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getDdr()
    {
        return $this->ddr;
    }

    public function getFrequency()
    {
        return $this->frequency;
    }
}
