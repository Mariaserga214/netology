<?php

error_reporting(E_ALL);
ini_set('dispay_errors', 1);

session_start();

$host = 'localhost';
$user = 'root';
$password = '';
$database = 'netology';

if (!array_key_exists('name', $_GET)) {
    header('Location: ./index.php');
}

$user = 'root';
$pass = '';
$pdo = new PDO('mysql:host=localhost;dbname=netology', $user, $pass);

if (array_key_exists('action', $_GET)) {
    switch ($_GET['action']) {
        case 'delete':
            $query = $pdo->prepare('ALTER TABLE '.$_GET['name'].' DROP COLUMN '.$_GET['tablefield']);
            $query->execute();

            header("Location: ./table.php?name=".$_GET['name']);
            exit();
            break;
        case 'edit':
            $query = $pdo->prepare(
                'ALTER TABLE '.$_GET['name'].' MODIFY COLUMN '.$_GET['tablefield'].' '.$_POST['Type']
            );
            $query->execute();

            header("Location: ./table.php?name=".$_GET['name']);
            exit();
            break;
        default:
            break;
    }
}

$query = 'DESCRIBE '.$_GET['name'];
$result = $pdo->prepare($query);
$result->execute();

$table = $result->fetchAll(PDO::FETCH_ASSOC);

$types = ['int(11)', 'varchar(200)', 'boolean', 'datetime'];

?>

<!DOCTYPE html>
<html>
<head>
    <title>Таблица</title>
</head>
<body>
<table border="1">
    <tr>
        <th>Название столбца</th>
        <th>Тип данных</th>
        <th>Изменение типа данных</th>
        <th>Удаление</th>
    </tr>
    <?php foreach ($table as $tables): ?>
        <tr>
            <td><?php echo($tables['Field']); ?></td>
            <td><?php echo($tables['Type']); ?></td>
            <td>
                <form method="post" action="?action=edit&amp;name=<?php echo $_GET['name']; ?>&amp;tablefield=<?php echo $tables['Field']; ?>">
                    <select name="Type">
                        <?php foreach ($types as $type): ?>
                            <option value="<?php echo $type; ?>"><?php echo $type; ?> </option>
                        <?php endforeach; ?>
                    </select>
                    <input type="submit" name="change" value="Изменить тип данных">
                </form>
            </td>
            <td>
                <a href="?action=delete&amp;tablefield=<?php echo $tables['Field']; ?>&amp;name=<?php echo
                $_GET['name']; ?>">
                    Удалить
                </a>
            </td>
        </tr>
    <?php endforeach ?>
</table>

</body>
</html>
