<?php

session_start();

if (!array_key_exists('user', $_SESSION)) {
    header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
    exit('403я ошибка - чтобы мне было понятно, что она реально тут есть!');
}

$message = '';

if (array_key_exists('submit', $_POST)) {
    $save_path = __DIR__.'/tests/';
    $filename = $_FILES['f']['name'];

    if (move_uploaded_file($_FILES['f']['tmp_name'], $save_path.$filename)) {
        $message = 'Ваш файл успешно загружен';
        header("Location: ./list.php");

    } else {
        $message = 'Файл не был загружен, повторите попытку!';
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Отправка файла на сервер</title>
</head>
<body>

<?php if ($message): ?>
    <div><?php echo $message; ?></div>
<?php endif; ?>

<form enctype="multipart/form-data" method="post">
    <div>
        <label>
            Загрузите тест:
            <input type="file" name="f" accept=".json,application/json" required="required">
        </label>
    </div>
    <div>
        <input type="submit" name="submit" value="Отправить">
    </div>
</form>
</body>
</html>
