<?php

include __DIR__.'/config.php';

session_start();

$message = '';

if (!empty($_POST)) {
    if (array_key_exists('register', $_POST)) {
        $query = db()->prepare("SELECT * FROM user WHERE login = :login");
        $query->bindValue('login', $_POST['login']);
        $query->execute();
        $userdata = $query->fetchAll(PDO::FETCH_ASSOC);

        if ($userdata) {
            $message = 'Пользователь с таким логином уже существует';
        } else {
            $new_user = db()->prepare('INSERT INTO user (login, password) VALUES (:login, :password)');
            $new_user->bindValue('login', $_POST['login']);
            $new_user->bindValue('password', $_POST['password']);
            $new_user->execute();
        }
    }

    if (array_key_exists('log', $_POST) && array_key_exists('login', $_POST) && array_key_exists('password', $_POST)) {
        $userdata = db()->prepare("SELECT * FROM user WHERE login = :login AND password = :password");
        $userdata->bindValue('login', $_POST['login']);
        $userdata->bindValue('password', $_POST['password']);
        $userdata->execute();

        $userdata = $userdata->fetchAll(PDO::FETCH_ASSOC);

        if ($userdata) {

            $_SESSION['user'] = $userdata[0];
            header('Location: ./index.php');
        }

        $message = 'Неверно введен пароль!';
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Войти для прохождени тестов</title>
</head>
<body>
<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-xs-wrap">
                <h1>Войти для прохождения тестов</h1>

                <?php echo($message); ?>
                <br/>

                <form method="POST">
                    <div class="form-group">
                        <label for="lg" class="sr-only">Логин</label>
                        <input type="text" placeholder="Логин" name="login" id="lg" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label for="key" class="sr-only">Пароль</label>
                        <input type="password" placeholder="Пароль" name="password" id="key" class="form-control">
                    </div>
                    <input type="checkbox" id="guest" name="is_guest" value="guest"/>
                    <label for="guest">Войти как гость</label>
                    <br/>
                    <input name="log" type="submit" id="btn-login" class="btn btn-success btn-lg btn-block" value="Войти">
                    <br/>
                    <input type="submit" name="register" value="Зарегистрироваться"/>
                </form>
                <hr>
            </div>
        </div>
    </div>
</section>
</body>
</html>
