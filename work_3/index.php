<?php

$animals = [
	'Eurasia' => array("Canis lupus", "Mammuthus columbi", "Procyon lotor", "Ursidae"),
	'North America' => array("Grizzly bear", "Canis latrans", "Gulo gulo", "Mephitidae"),
	'South America' => array("Lama guanicoe", "Dasypus novemcinctus", "Hydrochoerus hydrochaeris", "Cingulata"),
	'Africa' => array("Hexaprotodon liberiensis", "Syncerus caffer", "Ceratotherium simum", "Elephantidae"),
	'Australia' => array("Ornithorhynchus anatinus", "Phascolarctos cinereus", "Sarcophilus harrisii", "Macropus"),
	'Antarctica' => array("Aptenodytes forsteri", "Mirounga leonina", "Orcinus orca", "Balaenidae"),
];

$two_word_animal = [];
$first_words = [];
$second_words = [];

foreach ($animals as $continent => $full_latin_name) {
	foreach ($full_latin_name as $animal) {
		$parts = explode(" ", $animal);

		if (count($parts) == 2) {
			$two_word_animal[] = $animal;
			$first_words[$continent][] = $parts[0];
			$second_words[] = $parts[1];
		}
	}
}

echo '<h2>' . 'Животные из двух слов' . '</h2>'."\n";
echo implode(', ', $two_word_animal)."\n";

$words = [];

shuffle($second_words);

foreach ($first_words as $continent => $animals) {
	foreach ($animals as $animal) {
		$words[$continent][] = $animal . ' ' . array_pop($second_words);
	}
}

foreach ($words as $continent => $word) {
	echo '<h2>' . $continent . '</h2>'."\n";
	echo implode(', ', $word)."\n";
}
