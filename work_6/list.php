<?php

$save_path = __DIR__.'/tests/';

$uploaded_files = scandir($save_path);

$tests = [];

foreach ($uploaded_files as $filename) {
	if (strpos($filename, '.') === 0) {
		continue;
	}

	$tests[] = $filename;
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Список загруженных тестов</title>
</head>
<body>
<ul>
	<?php foreach ($tests as $test): ?>
		<li><?php echo $test; ?></li>
	<?php endforeach; ?>
</ul>
</body>
</html>
