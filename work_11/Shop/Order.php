<?php

namespace Shop;

class Order
{
    public $products;
    public $totalsum;
    public $delivery;
    public $weight;
    public $deliveryPrice;

    public function createfromCart($Cart)
    {
        $this->totalsum = $Cart->subtotal();
        $this->products = $Cart->getproductslist();
        $this->weight = $Cart->subweight();
    }

    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;
    }

    public function completeOrder()
    {
        $this->deliveryPrice = $this->delivery->getCost($this->weight);
    }

    public function __toString()
    {
        $text = '';

        $text .= 'Продукты:'."\n";

        foreach ($this->products as $product) {
            $text .= $product."\n";
        }

        $text .= 'Сумма заказа: '.$this->totalsum."\n";
        $text .= 'Доставка: '.$this->delivery."\n";
        $text .= 'Вес: '.$this->weight."\n";
        $text .= 'Стоимость доставки:'.$this->deliveryPrice."\n";

        return $text;
    }
}
