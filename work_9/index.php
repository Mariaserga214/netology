<?php

//Первый класс "Машина"
class Car
{
	public $model;
	public $color;
	public $maxspeed;
	public $year;
	public $price;
	public $discount = 100000;

	public function __construct($model, $color, $maxspeed, $year, $price, $discount=NULL)
	{
		$this->model = $model;
		$this->color = $color;
		$this->maxspeed = $maxspeed;
		$this->year = $year;
		$this->price = $price;
	
		if ($discount) {
			$this->discount = $discount;
		}
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getPriceWithDiscount()
	{
		if ($this->discount) {
			return $this->price - $this->discount;
		}

		return $this->price;
	}

	public function setMaxSpeed($maxspeed)
	{
		if ($maxspeed < 0) {
			$maxspeed = 0;
		}

		$this->maxspeed = $maxspeed;
	}

	public function getMaxSpeedWithName()
	{
		return $this->maxspeed.'km/h';
	}
}

//Объекты Машина

$car1 = new Car('Audi A7', 'red', 250, '2012', 3200000);
$car2 = new Car('BMW X3', 'black', 245, '2014', 3500000);



echo $car1->getPrice()."\n";
print_r($car1);
echo 'Первое авто'."\br";

print_r($car2);

$current_price = 0;
$cars = [$car1, $car2];


foreach ($cars as $car) {
    if ($car->getPrice() > $current_price) {
        $current_price = $car->getPrice();
    }
}

echo $current_price;

//Второй класс "Телевизор"

class TVset
{
	public $model;
	public $color;
	public $diagonal;
	public $price;
	public $discount = 5000;//купон на 2000 руб. на любой телевизор от 30000

	public function __construct($model, $color, $diagonal, $price, $discount=NULL)
	{
		$this->model = $model;
		$this->color = $color;
		$this->diagonal = $diagonal;
		$this->price = $price;
		$this->discount = $discount;
		
		if ($discount) {
			$this->discount = $discount;
		}
	}


	public function getPrice()
	{
		return $this->price;
	}

	public function getPriceWithDiscount()
	{
		if ($this->discount) {
			return $this->price - $this->discount;
		}

		return $this->price;
	}

	public function isWidescreen()
	{
		return $this->diagonal > 45;
	}
}

//Объекты телевизор
$TVset1 = new TVset('Philips', 'grey', '50', 40000);
$TVset2 = new TVset('Samsung', 'black', '40', 27000);

echo $TVset1->getPrice()."\n";
print_r($TVset1);
print_r($TVset2);

//Третий класс "Шариковая ручка"	
class Pen
{
	public $label;
	public $color;
	public $diameter;
	public $price;
	public $discount;//5% на товары этой категории

	public function __construct($label, $color, $diameter, $price, $discount = null)
	{
		$this->label = $label;
		$this->color = $color;
		$this->diameter = $diameter;
		$this->price = $price;

		if ($discount) {
			$this->discount = $discount;
		}
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getPriceWithDiscount()
	{
		if ($this->discount) {
			return $this->price - $this->discount;
		}

		return $this->price;
	}

	public function isForStudent()
	{
		return $this->color == 'blue';
	}
}

$Pen1 = new Pen('BIC', 'blue', '0,9', 30);
$Pen2 = new Pen('Pilot', 'red', '0,6', 20);

echo $Pen1->getPrice()."\n";
print_r($Pen1);
print_r($Pen2);

//Четвертый класс "Утка"	
class Duck
{
	public $material;
	public $color;
	public $promo; //наличие подарка или нет
	public $price;
	public $discount; //10% на товары в этой категории

	public function __construct($material, $color, $promo, $price, $discount = null)
	{
		$this->material = $material;
		$this->color = $color;
		$this->promo = $promo;
		$this->price = $price;

		if ($discount) {
			$this->discount = $discount; 
		}
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getPriceWithDiscount()
	{
		if ($this->discount) {
			return $this->price - $this->discount;
		}

		return $this->price;
	}

	public function isPromo()
	{
		return $this->promo == 'yes';
	}
}

$Duck1 = new Duck('rubber', 'yellow', 'no', 100);
$Duck2 = new Duck('porcelain', 'white', 'yes', 1200);


echo $Duck1->getPrice()."\n";
print_r($Duck1);
print_r($Duck2);

//Пятый класс "Товар"	
class Product
{
	public $name;
	public $category;
	public $availability;
	public $price;
	public $discount = 100000;

	public function __construct($name, $category, $availability, $price, $discount = null)
	{
		$this->name = $name;
		$this->category = $category;
		$this->availability = $availability;
		$this->price = $price;

		if ($discount) {
			$this->discount = $discount;
		}
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getPriceWithDiscount()
	{
		if ($this->discount) {
			return $this->price - $this->discount;
		}

		return $this->price;
	}

	public function canBePurchased()
	{
		return $this->availability == 'yes' && $this->price > 0;
	}
}

$Product1 = new Product('Armchair Blue Sky', 'furniture', 'available', 12000);
$Product2 = new Product('Table Greenwood', 'furniture', 'not available', 5000);


echo $Product1->getPrice()."\n";
print_r($Product1);


