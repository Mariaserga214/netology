<?php

session_start();

if (!array_key_exists('user', $_SESSION)) {
	header($_SERVER["SERVER_PROTOCOL"]."http/1.1 403 Forbidden");
	exit('403я ошибка - чтобы мне было понятно, что она реально тут есть!');
}

$file_name = __DIR__.'/tests/'.$_GET['name'];

if (file_exists ($file_name)) {
	unlink ($file_name);
}

header('Location:./list.php');
