<?php

namespace Shop;

class Cart
{
    public $products;

    public function getproductslist()
    {
        return $this->products;
    }

    public function addProduct($product)
    {
        $this->products[] = $product;
    }

    public function count()
    {
        $result = count($this->products);

        return $result;
    }

    public function deleteProduct($product)
    {
        $deleteProduct = array_search($product, $this->products, true);
        unset($this->products[$deleteProduct]);
    }

    public function subtotal()
    {
        $totalprice = 0;

        foreach ($this->products as $product) {
            $price = $product->getPrice();
            $totalprice += $price;

        }

        return $totalprice;
    }

    public function subweight()
    {
        $totalweight = 0;

        foreach ($this->products as $product) {
            $weight = $product->getweight();
            $totalweight += $weight;

        }

        return $totalweight;
    }

    public function __toString()
    {
        $text = '';

        $text .= 'Продукты:'."\n";

        foreach ($this->products as $product) {
            $text .= $product."\n";
        }

        return $text;
    }
}
