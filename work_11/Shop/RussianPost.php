<?php

namespace Shop;

class RussianPost implements DeliveryType
{
    public $name;

    public function getName()
    {
        return $this->name;
    }

    public function getCost($weight)
    {
        return $weight * 1.5;
    }

    public function __toString()
    {
        return 'Почта России';
    }
}
