<?php

$user = 'root';
$pass = '';
$pdo = new PDO('mysql:host=localhost;dbname=netology', $user, $pass);

if (!array_key_exists('name', $_GET)) {
    $_GET['name'] = '';
}

if (!array_key_exists('author', $_GET)) {
    $_GET['author'] = '';
}

if (!array_key_exists('isbn', $_GET)) {
    $_GET['isbn'] = '';
}

$stmt = $pdo->prepare("SELECT * FROM books WHERE name LIKE :name AND author LIKE :author AND isbn LIKE :isbn");

$stmt->bindValue('name', '%'.$_GET['name'].'%', PDO::PARAM_STR);
$stmt->bindValue('author', '%'.$_GET['author'].'%', PDO::PARAM_STR);
$stmt->bindValue('isbn', '%'.$_GET['isbn'].'%', PDO::PARAM_STR);
$stmt->execute();

$tabledata = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html>
<head>
    <title>«Реляционные базы данных и SQL»</title>
</head>
<body>
<h3>Искать</h3>
<form action="" method="get">
    <input type="text" name="name" placeholder="Название" value="<?php echo $_GET['name']; ?>"/>
    <input type="text" name="author" placeholder="Автор" value="<?php echo $_GET['author']; ?>"/>
    <input type="text" name="isbn" placeholder="ISBN" value="<?php echo $_GET['isbn']; ?>"/>
    <input type="submit" value="Искать"/>
</form>
<br/>
<br/>
<br/>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Author</th>
        <th>Year</th>
        <th>ISBN</th>
        <th>Genre</th>
    </tr>
    <?php foreach ($tabledata as $data): ?>
        <tr>
            <td><?php echo $data['id']; ?></td>
            <td><?php echo $data['name']; ?></td>
            <td><?php echo $data['author']; ?></td>
            <td><?php echo $data['year']; ?></td>
            <td><?php echo $data['isbn']; ?></td>
            <td><?php echo $data['genre']; ?></td>
        </tr>
    <?php endforeach; ?>
</body>
</html>
