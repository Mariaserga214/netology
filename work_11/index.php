<?php

function my_autoloader($className)
{
    $filePath = __DIR__.DIRECTORY_SEPARATOR.str_replace('\\', DIRECTORY_SEPARATOR, $className).'.php';

    if (file_exists($filePath)) {
        include $filePath;
    }
}

spl_autoload_register('my_autoloader');

$PC1 = new Catalogue\PC('Samsung', 1000, 300, 50, '8Gb', '2.8Ghz');

$Cart = new Shop\Cart();

$Cart->addProduct($PC1);
$Cart->addProduct($PC1);
$Cart->addProduct($PC1);

echo 'Корзина:'."\n";
echo $Cart;

$Order1 = new Shop\Order();
$Order1->createfromCart($Cart);
$Order1->setDelivery(new Shop\Pickup());
$Order1->completeOrder();

echo 'Заказ:'."\n";
echo $Order1;
