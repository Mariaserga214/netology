<?php

namespace Catalogue;

class TVset extends Product implements Discount
{
    public $model;
    public $color;
    public $diagonal;

    public function __construct($model, $color, $diagonal, $price, $weight, $availability, $discount = null)
    {
        parent::__construct($model, 'tv', $availability, $price, 200, $weight, $discount);

        $this->model = $model;
        $this->color = $color;
        $this->diagonal = $diagonal;

        if ($discount && $weight > 10) {
            $this->discount = $discount;
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function isWidescreen()
    {
        return $this->diagonal > 45;
    }

    public function getDiscount()
    {
        return $this->discount;
    }
}
