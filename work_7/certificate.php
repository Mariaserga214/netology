<?php

header("Content-type: image/png");

$im = imagecreatetruecolor(400, 309);

$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
$cert_border = imagecreatefrompng(__DIR__. '/cert_border.png');
imagefilledrectangle($im, 0, 0, 399, 308, $white);

imagecopy($im, $cert_border, 0, 0, 0, 0, 400, 309);

// The text to draw

$text = 'Сертификат';
// Replace path by your own font path
$font = 'HelveticaThin.ttf';


// Add some shadow to the text

imagettftext($im, 20, 0, 11, 40, $grey, $font, $text);

// Add the text

imagettftext($im, 20, 0, 10, 40, $black, $font, $text);

$text = $_GET['username'].', ';

imagettftext($im, 15, 0, 10, 130, $black, $font, $text);

$text = $_GET['result'];

imagettftext($im, 15, 0, 10, 180, $black, $font, $text);

// Using imagepng() results in clearer text compared with imagejpeg()
imagepng($im);
imagedestroy($im);

?>