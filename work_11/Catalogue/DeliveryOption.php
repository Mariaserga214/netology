<?php

namespace Catalogue;

interface DeliveryOption
{
	public function orderDelivery();
}
