<?php

$message = '';

if (array_key_exists('submit', $_POST)) {
	$save_path = __DIR__.'/tests/';
	$filename = $_FILES['f']['name'];

	if (move_uploaded_file($_FILES['f']['tmp_name'], $save_path.$filename)) {
		$message = 'Ваш файл успешно загружен';
	} else {
		$message = 'Файл не был загружен, повторите попытку!';
	}
}

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
	<title>Отправка файла на сервер</title>
</head>
<body>

<?php if ($message): ?>
	<div><?php echo $message; ?></div>
<?php endif; ?>

<form enctype="multipart/form-data" method="post">
	<div>
		<label>
			Загрузите тест:
			<input type="file" name="f" accept=".json,application/json" required="required">
		</label>
	</div>
	<div>
		<input type="submit" name="submit" value="Отправить">
	</div>
</form> 
</body>
</html>
