<?php

namespace Catalogue;

class Car extends Product implements DeliveryOption
{
    public $model;
    public $color;
    public $maxspeed;
    public $year;

    public function __construct($model, $color, $maxspeed, $year, $price, $discount = null)
    {
        parent::__construct($model, 'car', true, $price, 2500, 1300, $discount);

        $this->model = $model;
        $this->color = $color;
        $this->maxspeed = $maxspeed;
        $this->year = $year;

        if ($discount) {
            $this->discount = $discount;
        }
    }

    public function getPriceWithDiscount()
    {
        if ($this->discount) {
            return $this->price - $this->discount;
        }

        return $this->price;
    }

    public function setMaxSpeed($maxspeed)
    {
        if ($maxspeed < 0) {
            $maxspeed = 0;
        }

        $this->maxspeed = $maxspeed;
    }

    public function getMaxSpeedWithName()
    {
        return $this->maxspeed.'km/h';
    }

    public function orderDelivery()
    {
        return 'Pickup';
    }
}
