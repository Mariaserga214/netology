<?php

class User
{
    public function findAll()
    {
        $query = Di::get()->db()->prepare("SELECT * from user");
        $query->execute();

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findByLoginAndPassword($login, $password)
    {
        $userdata = Di::get()->db()->prepare("SELECT * FROM user WHERE login = :login AND password = :password");
        $userdata->bindValue('login', $_POST['login']);
        $userdata->bindValue('password', $_POST['password']);
        $userdata->execute();

        return $userdata->fetchAll(PDO::FETCH_ASSOC);
    }
}
