<?php

$animals = [
	'Eurasia' => array("Canis lupus", "Mammuthus columbi", "Procyon lotor", "Ursidae"),
	'North America' => array("Grizzly bear", "Canis latrans", "Gulo gulo", "Mephitidae"),
	'South America' => array("Lama guanicoe", "Dasypus novemcinctus", "Hydrochoerus hydrochaeris", "Cingulata"),
	'Africa' => array("Hexaprotodon liberiensis", "Syncerus caffer", "Ceratotherium simum", "Elephantidae"),
	'Australia' => array("Ornithorhynchus anatinus", "Phascolarctos cinereus", "Sarcophilus harrisii", "Macropus"),
	'Antarctica' => array("Aptenodytes forsteri", "Mirounga leonina", "Orcinus orca", "Balaenidae"),
	
];

$two_word_animal = [];

foreach ($animals as $k => $v) {
	foreach ($v as $animal) {
		$parts = explode(" ", $animal);

		if (count($parts) == 2) {
			$two_word_animal[] = $animal;
		}
	}
}

echo '<h2>All animals with two word names</h2>';
echo implode(', ', $two_word_animal);

$first_words = [];
$seconds_words = [];

foreach ($animals as $cont => $v) {
	foreach ($v as $animal) {
		$parts = explode(" ", $animal);

		$first_words[$cont][] = $parts[0];

		if (array_key_exists(1, $parts)) {
			$seconds_words[] = $parts[1];
		}
	}
}

$words = [];

foreach ($first_words as $cont => $animals) {
	foreach ($animals as $animal) {
		$words[$cont][] = $animal . ' ' . $seconds_words[rand(0, count($seconds_words) - 1)];
	}
}

foreach ($words as $cont => $word) {
	echo '<h2>' . $cont . '</h2>';
	echo implode(', ', $word)."\n";
}
